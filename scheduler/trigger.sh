#!/bin/bash

run_job() {
    DATA="{ \"Meta\": {\"PARENT_JOB\": \"koji-regenrepo-scheduler\"}, \"Payload\": \"`echo -n "$1" | base64 -w 0`\"}"

    curl -H "X-Nomad-Token: `cat /secrets/token`" \
        -X POST \
        --cacert /secrets/nomad-ca.pem \
        --data "$DATA" \
        ${NOMAD_ADDR}/v1/job/${REGEN_JOB}/dispatch

    return $?
}

run_job '{ "REPO_REGEX": ".*" }'
run_job '{ "REPO_REGEX": ".*", "KOJI_PROFILE": "kojitest" }'
