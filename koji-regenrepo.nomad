job "${PREFIX}_koji-regenrepo" {
  datacenters = ["*"]

  type = "batch"

  spread {
    attribute = "${node.unique.name}"
  }

  parameterized {
    payload = "required"
    meta_required = ["PARENT_JOB"]
  }

  reschedule {
    attempts       = 276
    interval       = "23h"
    unlimited      = false
    delay          = "5m"
    delay_function = "constant"
  }

  priority = 60

  task "${PREFIX}_koji-regenrepo" {
    driver = "docker"

    config {
      image = "https://gitlab-registry.cern.ch/linuxsupport/cronjobs/koji-regenrepo/koji-regenrepo:${CI_COMMIT_SHORT_SHA}"
      logging {
        config {
          tag = "${PREFIX}_koji-regenrepo"
        }
      }
      volumes = [
        "/etc/auth/koji_password:/auth/koji_password",
        "$REGEN_CACHE:/cache",
      ]
    }

    dispatch_payload {
      file = "config.json"
    }

    env {
      KOJI_PROFILE = "$KOJI_PROFILE"
      TAG = "${PREFIX}_koji-regenrepo"
    }

    resources {
      cpu = 100 # Mhz
      memory = 128 # MB
    }
  }
}
