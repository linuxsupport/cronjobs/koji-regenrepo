#!/bin/bash

CONFIG='/local/config.json'

if [ ! -f $CONFIG ]; then
  echo "Error: no config file found"
  exit -1
fi

# Read the job parameters
REPO_REGEX="`jq -r '.REPO_REGEX' $CONFIG`"
TAG_REGEX="`jq -r '.TAG_REGEX' $CONFIG`"
FORCE="`jq -r '.FORCE // "false"' $CONFIG`"
PROFILE="`jq -r ".KOJI_PROFILE // \\"${KOJI_PROFILE}\\"" $CONFIG`"
SUPPORTED_ARCHES="`jq -r '.ARCHES // "x86_64 i386 aarch64"' $CONFIG`"

KOJI="/usr/bin/koji -p ${PROFILE}"
REPOS=`${KOJI} list-external-repos --quiet`
REPO_NAMES=`printf "${REPOS}" | cut -d' ' -f1 | grep -e "${REPO_REGEX}" | xargs | sed 's/ /\\\|/g'`

TAGS=`${KOJI} list-tags`
TAG_NAMES=`printf "${TAGS}" | grep -e "${TAG_REGEX}" | xargs`

if [[ -z "$REPO_NAMES" && -z "$TAG_NAMES" ]]; then
  echo "No external repos match the regex /${REPO_REGEX}/ and no tags match the regex /${TAG_REGEX}/."
  exit
fi

UPDATED_REPOS=""
for REPODATA in `printf "${REPOS}" | grep -e "^\(${REPO_NAMES}\) " | awk '{print $1 "#" $2}'`; do
  NAME=`echo "${REPODATA}" | cut -d'#' -f1`
  URL=`echo "${REPODATA}" | cut -d'#' -f2`

  for ARCH in $SUPPORTED_ARCHES; do
    REPO_URL=`echo $URL | sed "s/\\\$arch/${ARCH}/g"`

    ATTEMPT_COUNTER=0
    while [[ $ATTEMPT_COUNTER -lt 3 ]]; do
      ATTEMPT_COUNTER=$(($ATTEMPT_COUNTER+1))

      STATUS=$(curl --silent -o /dev/null -IL -w "%{http_code}\n" "${REPO_URL}/repodata/repomd.xml")
      [ $STATUS -eq "200" ] && break

      echo "Couldn't retrieve ${REPO_URL}/repodata/repomd.xml, status code $STATUS."
      sleep ${ATTEMPT_COUNTER}s
    done
    if [[ $STATUS -ne "200" ]]; then
      echo "Retry limit reached. Skipping $NAME $ARCH."
      continue
    fi

    sha=`curl --silent -L "${REPO_URL}/repodata/repomd.xml" | sha256sum`

    if [[ "$FORCE" != true && -e /cache/$NAME.$ARCH.sha256sum ]]; then
      oldsha=`cat /cache/$NAME.$ARCH.sha256sum`
      if [[ "$sha" == "$oldsha" ]]; then
        echo "$NAME $ARCH not updated."
        continue
      fi
    fi

    echo "External repo $NAME $ARCH updated"
    UPDATED_REPOS="$UPDATED_REPOS $NAME"
    echo "$sha" > /cache/$NAME.$ARCH.sha256sum
  done
done

UPDATED_BUILDROOTS=""
for REPO in $UPDATED_REPOS; do
  br=`${KOJI} list-external-repos --name=${REPO} --quiet --used | cut -d' ' -f1 | xargs`
  [[ ! -z "$br" ]] && UPDATED_BUILDROOTS="$UPDATED_BUILDROOTS $br"
done

# The awk magic filters duplicates without sorting the list
for TAG in `echo "${UPDATED_BUILDROOTS} ${TAG_NAMES}" | tr ' ' '\n' | awk '!seen[$0]++'`; do
  inheritance=`${KOJI} list-tag-inheritance ${TAG} --reverse | wc -l`
  WAIT=""
  if [[ $inheritance -eq 1 ]]; then
    # If no other tag inherits from this one, we don't need to wait
    WAIT="--nowait"
  fi
  echo "Triggering regen-repo for ${TAG}"
  ${KOJI} \
    --user koji \
    --authtype password \
    --password `cat /auth/koji_password` \
    regen-repo \
    $WAIT \
    $TAG
done
