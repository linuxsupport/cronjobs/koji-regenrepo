job "${PREFIX}_koji-regenrepo-scheduler" {
  datacenters = ["*"]

  type = "batch"

  spread {
    attribute = "${node.unique.name}"
  }

  periodic {
    cron = "${SCHEDULE}"
    time_zone = "Europe/Zurich"
    prohibit_overlap = true
  }

  reschedule {
    attempts       = 276
    interval       = "23h"
    unlimited      = false
    delay          = "5m"
    delay_function = "constant"
  }

  task "${PREFIX}_koji-regenrepo-scheduler" {
    driver = "docker"

    config {
      image = "https://gitlab-registry.cern.ch/linuxsupport/cronjobs/koji-regenrepo/scheduler:${CI_COMMIT_SHORT_SHA}"
      logging {
        config {
          tag = "${PREFIX}_koji-regenrepo-scheduler"
        }
      }
      volumes = [
        "/etc/nomad/submit_token:/secrets/token",
        "/etc/nomad/ca.pem:/secrets/nomad-ca.pem"
      ]
    }

    env {
      NOMAD_ADDR = "$NOMAD_ADDR"
      NOMAD_CACERT = "/secrets/nomad-ca.pem"
      REGEN_JOB = "${PREFIX}_koji-regenrepo"
      TAG = "${PREFIX}_koji-regenrepo-scheduler"
    }

    resources {
      cpu = 100 # Mhz
      memory = 128 # MB
    }
  }
}
